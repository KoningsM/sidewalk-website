﻿using System;
using Sitecore.Analytics.Model.Framework;

namespace sidewalk_sitecore_8.Facets
{
    public interface ISubscriberData : IFacet
    {
        string Salutation { get; set; }
        Guid SidewalkContact { get; set; }
        string CompanyName { get; set; }
    }
}
