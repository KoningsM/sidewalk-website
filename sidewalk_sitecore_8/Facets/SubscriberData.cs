﻿using System;
using Sitecore.Analytics.Model.Framework;

namespace sidewalk_sitecore_8.Facets
{
    public class SubscriberData : Facet, ISubscriberData
    {
        private const string FieldSalutation = "Salutation";
        private const string FieldSidewalkContact = "SidewalkSignature";
        private const string FieldCompanyName = "CompanyName";

        public SubscriberData()
        {
            base.EnsureAttribute<string>(FieldSalutation);
            base.EnsureAttribute<Guid>(FieldSidewalkContact);
            base.EnsureAttribute<string>(FieldCompanyName);
        }

        public string Salutation {
            get { return base.GetAttribute<string>(FieldSalutation); }
            set { base.SetAttribute<string>(FieldSalutation, value); }
        }
        public Guid SidewalkContact {
            get { return base.GetAttribute<Guid>(FieldSidewalkContact); }
            set { base.SetAttribute<Guid>(FieldSidewalkContact, value); }
        }
        public string CompanyName {
            get { return base.GetAttribute<string>(FieldCompanyName); }
            set { base.SetAttribute<string>(FieldCompanyName, value); }
        }
    }
}