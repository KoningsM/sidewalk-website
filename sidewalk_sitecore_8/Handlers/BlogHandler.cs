﻿using System;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Events;

namespace sidewalk_sitecore_8.Handlers
{
    public class BlogHandler
    {
        public void OnItemSaved(object sender, EventArgs args)
        {
            var newItem = Event.ExtractParameter(args, 0) as Item;

            if (newItem != null)
            {
                newItem.Editing.BeginEdit();
                var date = string.IsNullOrEmpty(newItem["Date"]) ? DateUtil.IsoNowDate : newItem["Date"];
                newItem.Publishing.PublishDate = DateUtil.IsoDateToDateTime(newItem["Date"]);
                newItem.Editing.EndEdit();
            }
        }
    }
}