﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using sidewalk_sitecore_8.Facets;
using Sitecore.Analytics.Data;
using Sitecore.Analytics.DataAccess;
using Sitecore.Analytics.Model;
using Sitecore.Analytics.Model.Entities;
using Sitecore.Analytics.Tracking;
using Sitecore.Configuration;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.ListManagement.ContentSearch;
using Sitecore.ListManagement.ContentSearch.Model;
using Sitecore.StringExtensions;
using ContactData = Sitecore.ListManagement.ContentSearch.Model.ContactData;

namespace sidewalk_sitecore_8.sitecore.shell.Applications.ContactsManager
{
    public partial class Upload : Page
    {
        const string FacetEmails = "Emails";
        const string FacetEmailPreferred = "Preferred";
        const string FacetSubscriptionContact = "SubscriberData";
        private readonly Dictionary<string, int> _headersDictionary = new Dictionary<string, int>();
        private readonly char _seperator = ',';

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Files["UploadedFile"] != null)
            {
                HttpPostedFile myFile = Request.Files["UploadedFile"];

                var stream = new StreamReader(myFile.InputStream);
                var listContacts = new List<ContactData>();

                //Setup headers consts
                if (!stream.EndOfStream)
                {
                    SetHeaders(stream.ReadLine());
                }

                //Create contacts
                while (!stream.EndOfStream)
                {
                    var contactString = stream.ReadLine();
                    listContacts.Add(CreateContactFromString(contactString));
                }

                //Create list optionally
                var recipientListValue = Request.Form["recipientList"];
                if (!recipientListValue.IsNullOrEmpty())
                {
                    CreateExmList(recipientListValue, listContacts);
                }
            }
        }

        private void SetHeaders(string headerString)
        {
            var headers = headerString.Split(_seperator);

            for (var i = 0; i < headers.Length; i++)
            {
                switch (headers[i].Trim())
                {
                    case "First Name":
                        _headersDictionary.Add("First Name", i);
                        break;
                    case "Last Name":
                        _headersDictionary.Add("Last Name", i);
                        break;
                    case "Email":
                        _headersDictionary.Add("Email", i);
                        break;
                    case "Gender":
                        _headersDictionary.Add("Gender", i);
                        break;
                    case "Title":
                        _headersDictionary.Add("Title", i);
                        break;
                    case "Salutation":
                        _headersDictionary.Add("Salutation", i);
                        break;
                    case "Company Name":
                        _headersDictionary.Add("Company Name", i);
                        break;
                    case "Sidewalk Contact":
                        _headersDictionary.Add("Sidewalk Contact", i);
                        break;
                    //Uknown element
                }
            }
        }

        private ContactData CreateContactFromString(string contactString)
        {
            var contactArray = contactString.Split(_seperator);
            var newContact = CreateContact(contactArray[_headersDictionary["Email"]].Trim(),
                contactArray[_headersDictionary["First Name"]].Trim(),
                contactArray[_headersDictionary["Last Name"]].Trim(),
                contactArray[_headersDictionary["Gender"]].Trim(),
                contactArray[_headersDictionary["Email"]].Trim(),
                contactArray[_headersDictionary["Title"]].Trim(),
                contactArray[_headersDictionary["Salutation"]].Trim(),
                contactArray[_headersDictionary["Company Name"]].Trim(),
                contactArray[_headersDictionary["Sidewalk Contact"]].Trim());

            return newContact;
        }

        private ContactData CreateContact(string identifier, string firstName, string lastName, string gender ,string emailAddress, string title, string salutation, string companyName, string sidewalkContact)
        {
            if (String.IsNullOrEmpty(identifier)) return null;
            var contactRepo = Factory.CreateObject("contactRepository", true) as ContactRepository;
            if (contactRepo == null)
            {
                return null;
            }
            LeaseOwner leaseOwner = new LeaseOwner(identifier, LeaseOwnerType.OutOfRequestWorker);

            Contact newContact;

            Guid sidewalkContactGuid = GetSidewalkContact(sidewalkContact);

            LockAttemptResult<Contact> result = contactRepo.TryLoadContact(identifier, leaseOwner, TimeSpan.FromMinutes(1.0));
            switch (result.Status)
            {
                case LockAttemptStatus.Success:
                    // Existing Contact
                    newContact = result.Object;

                    // Email Facet
                    var emailFacet = newContact.GetFacet<IContactEmailAddresses>(FacetEmails);
                    if (!string.IsNullOrEmpty(emailFacet.Preferred))
                    {
                        IEmailAddress address = emailFacet.Entries[emailFacet.Preferred];
                        address.SmtpAddress = emailAddress;
                    }
                    emailFacet.Preferred = FacetEmailPreferred;

                    //Subscription Contact Facet
                    var subscriberData = newContact.GetFacet<ISubscriberData>(FacetSubscriptionContact);
                    subscriberData.Salutation = salutation;
                    subscriberData.CompanyName = companyName;
                    subscriberData.SidewalkContact = sidewalkContactGuid;

                    break;
                case LockAttemptStatus.NotFound:
                    // New Contact
                    ID guid = Sitecore.Data.ID.NewID;
                    newContact = contactRepo.CreateContact(guid);
                    newContact.Identifiers.Identifier = identifier;

                    // Email Facet
                    var subscriberDataNew = newContact.GetFacet<IContactEmailAddresses>(FacetEmails);
                    subscriberDataNew.Entries.Create(FacetEmailPreferred).SmtpAddress = emailAddress;
                    subscriberDataNew.Preferred = FacetEmailPreferred;

                    //Subscription Contact Facet
                    var eXmContactFacetNew = newContact.GetFacet<ISubscriberData>(FacetSubscriptionContact);
                    eXmContactFacetNew.Salutation = salutation;
                    eXmContactFacetNew.CompanyName = companyName;
                    eXmContactFacetNew.SidewalkContact = sidewalkContactGuid;

                    break;
                default:
                    throw new Exception("Couldn't get a lock on the database");
            }

            // Create and Update same once we have the contact
            var personalInfoNew = newContact.GetFacet<IContactPersonalInfo>("Personal");
            personalInfoNew.FirstName = firstName;
            personalInfoNew.Surname = lastName;
            personalInfoNew.Gender = gender;
            personalInfoNew.Title = title;

            newContact.ContactSaveMode = ContactSaveMode.AlwaysSave;
            try
            {
                contactRepo.SaveContact(newContact, new ContactSaveOptions(true, leaseOwner));
            }
            catch (Exception)
            {
                //if (ExContactNotSaved Error is exists and not testing for existence)
            }

            return new ContactData()
            {
                Identifier = identifier
            };
        }

        private Guid GetSidewalkContact(string sidewalkContact)
        {
            //SitecoreIndexableItem indexableItem = new SitecoreIndexableItem(Sitecore.Context.Item);
            var index = ContentSearchManager.GetIndex("sitecore_web_index");

            using (var context = index.CreateSearchContext())
            {
                // Initiate query - use custsom SitecoreItem class
                IQueryable<SearchResultItem> query = context.GetQueryable<SearchResultItem>();

                // Append standard query parameters - e.g. language, template, root item
                // and get results object
                var results = query
                             .Where(x => x.Name == sidewalkContact)
                             .GetResults();

                if (results != null)
                {
                    if (results.Hits.Any())
                    {
                        var contactMatches = results.Hits.Select(x => x.Document);
                        var firstContact = contactMatches.FirstOrDefault();
                        if (firstContact != null) return firstContact.ItemId.Guid;
                    }
                }
            }
            return Guid.Empty;
        }

        private void CreateExmList(string listName, IEnumerable<ContactData> listContacts)
        {
            var listManager = Factory.CreateObject("contactListManager", false) as ContactListManager;
            var list = new ContactList(listName);
            if (listManager == null)
            {
                throw new NullReferenceException("ListManager is null");
            }

            //Create list if necessary
            listManager.Create(list);
            //var listId = list.Id; //is assigned after creation.
            listManager.AssociateContacts(list, listContacts);
        }
    }
}