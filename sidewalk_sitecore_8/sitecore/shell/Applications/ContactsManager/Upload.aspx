﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="sidewalk_sitecore_8.sitecore.shell.Applications.ContactsManager.Upload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Contact Upload | Sidewalk</title>
    <link rel="stylesheet" href="/css/bootstrap.css"/>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <h1>Contact upload</h1>
            <p>This is the place where you can upload new contacts (in CSV format) to use inside of the Email Experience Manager. 
                Keep in mind that the following fields should be placed inside the csv file:</p>
            <ul>
                <li>First Name</li>
                <li>Last Name</li>
                <li>Email</li>
                <li>Gender</li>
                <li>Title</li>
                <li>Salutation</li>
                <li>Company Name</li>
                <li>Sidewalk Contact</li>
            </ul>
            
            <form action="Upload.aspx" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="recipientList">Enter a name for the Recipient list if you wish to put the contacts directly into one.</label>
                    <input type="text" class="form-control" id="recipientList" name="recipientList" runat="server" placeholder="Recipient List"/>
                </div>

                <div class="form-group">
                    <label for="UploadedFile">File input</label>
                    <input type="file" id="UploadedFile" name="UploadedFile"/>
                    <p class="help-block">List in csv format with header line</p>
                </div>

                <input type="submit" class="btn btn-default pull-right" value="Upload" />
            </form>
        </div>
    </div>
</div>
</body>
</html>
