﻿using System.Web.Mvc;

namespace sidewalk_sitecore_8.Controllers
{
    public class HomePageController : Controller
    {
        // GET: HomePage
        public ActionResult Index()
        {
            return View();
        }
    }
}