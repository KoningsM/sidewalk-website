﻿using System;
using System.Web.Mvc;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;

namespace sidewalk_sitecore_8.Controllers
{
    public class BaseController : Controller
    {
        /// <summary>
        /// Returns the DataSource Item specified by the user when Sublayout is added to the page.  If not specified, returns null
        /// </summary>
        public Item DataSourceItem
        {
            get
            {
                try
                {
                    var str = RenderingContext.Current.Rendering.DataSource;
                    return string.IsNullOrEmpty(str) ? null : RenderingContext.Current.Rendering.Item;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
    }
}