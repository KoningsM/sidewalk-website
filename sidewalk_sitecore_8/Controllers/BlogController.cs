﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using sidewalk_sitecore_8.Indexing;
using sidewalk_sitecore_8.Models.Blog;
using Sitecore;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.ContentSearch.Utilities;
using Sitecore.Links;
using Sitecore.Mvc.Presentation;
using static System.String;
using Constants = sidewalk_sitecore_8.Utils.Constants;

namespace sidewalk_sitecore_8.Controllers
{
    public class BlogController : BaseController
    {
        // GET: BlogOverview
        public ActionResult BlogOverview()
        {
            var blogoverviewModel = new BlogOverviewModel { Blogs = new List<BlogPostModel>() };
            string indexName = $"sitecore_{Context.Item.Database.Name}_index";
            var index = ContentSearchManager.GetIndex(indexName);

            using (var context = index.CreateSearchContext())
            {
                var bucketId = Constants.BlogPostsBucketId;

                var authorName = Request.QueryString[Constants.AuthorQueryString] ?? Empty;
                var author = context.GetQueryable<SearchResultItem>()
                    .Where(item => item.TemplateId == Constants.BlogAuthorTemplateId)
                    .FirstOrDefault(item => item["Name"] == authorName);
                var authorId = author?.ItemId.ToGuid().ToString("n");
                

                var tagName = Request.QueryString[Constants.TagQueryString] ?? Empty;
                var tag = context.GetQueryable<SearchResultItem>()
                    .Where(item => item.TemplateId == Constants.BlogTagTemplateId)
                    .FirstOrDefault(item => item["Name"] == tagName);
                var tagId = tag?.ItemId.ToGuid().ToString("n");


                var blogpostItems = context.GetQueryable<BlogPostResultItem>()
                    .Where(item => item.Paths.Contains(bucketId))
                    .Where(item => item.TemplateId == Constants.BlogPostTemplateId);

                if (!IsNullOrEmpty(tagId))
                {
                    blogpostItems = blogpostItems.Where(item => item.Tags.Contains(tagId));
                }

                if (!IsNullOrEmpty(authorId))
                {
                    blogpostItems = blogpostItems.Where(item => item.Author == authorId);
                }

                blogoverviewModel.TotalBlogPosts = blogpostItems.Count();

                var itemsToSkipString = RenderingContext.Current.Rendering.Parameters["Number of blogs to show initially"];
                int itemsToSkip;

                int.TryParse(itemsToSkipString, out itemsToSkip);

                blogpostItems = blogpostItems
                    .OrderByDescending(item => item.SortingDate)
                    .Take(itemsToSkip);

                var result = blogpostItems.GetResults();
                var resultItemList = result.Hits.Select(h => h.Document).ToList();

                var blogposts = resultItemList.Select(blogpostitem => new BlogPostModel { DatasourceItem = blogpostitem.GetItem() }).ToList();

                if (blogposts.Any()) blogoverviewModel.Blogs = blogposts;
            }

            return View("/Views/Renderings/Sidewalk/Blog/BlogOverview.cshtml", blogoverviewModel);
        }

        public ActionResult BlogRedirect()
        {
            var blogpostmodel = new BlogOverviewModel();

            string indexName = $"sitecore_{Context.Item.Database.Name}_index";
            var index = ContentSearchManager.GetIndex(indexName);

            using (var context = index.CreateSearchContext())
            {
                var bucketId = Constants.BlogPostsBucketId;
                
                var blogpostItems = context.GetQueryable<BlogPostResultItem>()
                    .Where(item => item.Paths.Contains(bucketId))
                    .Where(item => item.TemplateId == Constants.BlogPostTemplateId)
                    .OrderByDescending(item => item.SortingDate)
                    .Take(2);

                var result = blogpostItems.GetResults();
                var resultList = result.Select(r => r.Document.GetItem());
                var blogposts = resultList.Select(blogpost => new BlogPostModel { DatasourceItem = blogpost }).ToList();

                blogpostmodel.Blogs = blogposts;
            }
            
            return View("/Views/Renderings/Sidewalk/Blog/BlogRedirect.cshtml", blogpostmodel);
        }

        public ActionResult BlogPost()
        {
            var blogpostmodel = new BlogPostModel();

            if (HttpContext.Request.Url != null)
            {
                var urlParts = HttpContext.Request.Url.AbsolutePath.Split('/');
                var itemName = urlParts[urlParts.Length - 1];

                var bucketId = Constants.BlogPostsBucketId;

                string indexName = $"sitecore_{Context.Item.Database.Name}_index";
                var index = ContentSearchManager.GetIndex(indexName);
                using (var context = index.CreateSearchContext())
                {

                    var blogpost = context.GetQueryable<BlogPostResultItem>()
                            .Where(item => item.Paths.Contains(bucketId))
                            .FirstOrDefault(item => item.Name == itemName);

                    var blogpostitem = blogpost?.GetItem();
                    if (blogpostitem != null)
                    {
                        blogpostmodel.DatasourceItem = blogpostitem;
                    }
                }
            }

            var blogpage = Context.Database.GetItem(Constants.BlogPageId);
            if (blogpostmodel.DatasourceItem == null && blogpage != null)
            {
                Response.Redirect(LinkManager.GetItemUrl(blogpage));
            }

            return View("/Views/Renderings/Sidewalk/Blog/BlogPost.cshtml", blogpostmodel);
        }

        public ActionResult RelatedBlogPosts()
        {
            var blogoverviewModel = new BlogOverviewModel { Blogs = new List<BlogPostModel>() };
            if (HttpContext.Request.Url != null)
            {
                string indexName = $"sitecore_{Context.Item.Database.Name}_index";
                var index = ContentSearchManager.GetIndex(indexName);

                using (var context = index.CreateSearchContext())
                {
                    // Get current blogpost by url
                    var urlParts = HttpContext.Request.Url.AbsolutePath.TrimEnd('/').Split('/');
                    var itemName = urlParts[urlParts.Length - 1];
                    var currentBlogpost = context.GetQueryable<BlogPostResultItem>()
                            .Where(item => item.Paths.Contains(Constants.BlogPostsBucketId))
                            .First(item => item.Name == itemName);

                    var blogpostitem = currentBlogpost.GetItem();
                    if (blogpostitem != null)
                    {
                        var tagItems = blogpostitem["Tags"].Split('|');
                        foreach (var tag in tagItems)
                        {
                            if (!IsNullOrEmpty(tag))
                            {
                                var shortId = IdHelper.NormalizeGuid(tag);

                                var bucketId = Constants.BlogPostsBucketId;
                                var blogpostItems = context.GetQueryable<BlogPostResultItem>()
                                    .Where(item => item.Paths.Contains(bucketId))
                                    .Where(item => item.TemplateId == Constants.BlogPostTemplateId)
                                    .Where(item => item.ItemId != currentBlogpost.ItemId)
                                    .Where(item => item.Tags.Contains(shortId))
                                    .OrderByDescending(item => item.SortingDate)
                                    .Take(2);

                                var result = blogpostItems.GetResults();
                                var resultItemList = result.Hits.Select(h => h.Document).ToList();

                                var blogposts =
                                    resultItemList.Select(
                                        blogpost => new BlogPostModel { DatasourceItem = blogpost.GetItem() })
                                        .ToList();

                                if (blogposts.Any()) blogoverviewModel.Blogs.AddRange(blogposts);

                            }
                        }
                        return PartialView("/Views/Renderings/Sidewalk/Blog/RelatedBlogPosts.cshtml", blogoverviewModel);
                    }
                }
                return null;
            }
            return null;
        }
    }
}