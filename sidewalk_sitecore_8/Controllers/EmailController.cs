﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using sidewalk_sitecore_8.Facets;
using sidewalk_sitecore_8.Pipelines;
using Sitecore.Analytics;
using Sitecore.Data;
using Sitecore.Modules.EmailCampaign;
using Sitecore.Modules.EmailCampaign.Core;
using Sitecore.Web;

namespace sidewalk_sitecore_8.Controllers
{
    public class EmailController : Controller
    {
        private string SubscriberDataFacet { get; set; }

        // GET: Email
        public ActionResult SidewalkSignature()
        {
            Sitecore.Data.Items.Item sidewalkContact = null;
            var webDb = Sitecore.Configuration.Factory.GetDatabase("web");

            if (Tracker.Current != null)
            {
                var contact = Tracker.Current.Contact;
                var facet = contact.GetFacet<ISubscriberData>("SubscriberData");
                if (facet != null)
                {
                    sidewalkContact = webDb.GetItem(new ID(facet.SidewalkContact));
                }
            }

            if (sidewalkContact == null)
            {
                sidewalkContact = webDb.GetItem("{74533132-56CD-4D99-863D-C6FF00EC366D}");
            }

            return View(sidewalkContact);
        }

        private static ID GetDecryptedContactId()
        {
            var encryptedContactIdValue = WebUtil.GetQueryString("ec_contact_id");

            if (string.IsNullOrEmpty(encryptedContactIdValue))
            {
                return ID.Null;
            }

            var messageRootItemId = WebUtil.GetQueryString("sc_itemid");
            var db = WebUtil.GetQueryString("ec_database");

            if (string.IsNullOrEmpty(messageRootItemId) || string.IsNullOrEmpty(db))
            {
                return ID.Null;
            }

            var messageRootItem = Database.GetDatabase(db).GetItem(ID.Parse(messageRootItemId));
            if (messageRootItem == null)
            {
                return ID.Null;
            }

            var messageIdValue = messageRootItem.ParentID.ToString();

            ShortID messageShortId = ShortID.Parse(messageIdValue);
            ShortID encryptedContactShortId = ShortID.Parse(encryptedContactIdValue);

            Guid contactId;

            using (var provder = new GuidCryptoServiceProvider(Encoding.UTF8.GetBytes(GlobalSettings.PrivateKey), messageShortId.Guid.ToByteArray()))
            {
                contactId = provder.Decrypt(encryptedContactShortId.Guid);
            }
            return ID.Parse(contactId);
        }
    }
}