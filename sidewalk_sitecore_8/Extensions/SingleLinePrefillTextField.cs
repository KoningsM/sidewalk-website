﻿using System.ComponentModel;
using System.ComponentModel.Design;
using System.Linq;
using System.Web;
using Sitecore.Analytics;
using Sitecore.Analytics.Model.Entities;
using Sitecore.Form.Core.Attributes;

namespace sidewalk_sitecore_8.Extensions
{
    [Designer("System.Windows.Forms.Design.ParentControlDesigner, System.Design", typeof(IDesigner))]
    public class SingleLinePrefillTextField : Sitecore.Form.Web.UI.Controls.SingleLineText
    {
        [VisualCategory("Prefill")]
        [VisualProperty("Session Key", 100), DefaultValue("")]
        public string SessionKey { get; set; }

        private bool _isHidden = true;

        [VisualCategory("Prefill")]
        [VisualFieldType(typeof(Sitecore.Form.Core.Visual.BooleanField))]
        [VisualProperty("Is Hidden?", 300), DefaultValue("No")]
        public string IsHidden
        {
            get
            {
                return _isHidden ? "Yes" : "No";
            }
            set
            {
                _isHidden = value == "Yes";
            }
        }

        protected override void OnLoad(System.EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(SessionKey))
            {
                if (Tracker.Current != null)
                {
                    var personalFacet = Tracker.Current.Contact.GetFacet<IContactPersonalInfo>("Personal");
                    var emailFacet = Tracker.Current.Contact.GetFacet<IContactEmailAddresses>("Emails");
                    if (personalFacet != null)
                    {
                        switch (SessionKey)
                        {
                            case "firstname":
                                Text = personalFacet.FirstName;
                                break;
                            case "lastname":
                                Text = personalFacet.Surname;
                                break;
                            case "jobtitle":
                                Text = personalFacet.JobTitle;
                                break;
                            case "email":
                                if (!string.IsNullOrEmpty(emailFacet.Preferred))
                                {
                                    IEmailAddress emailAddress = emailFacet.Entries[emailFacet.Preferred];
                                    if (!string.IsNullOrWhiteSpace(emailAddress.SmtpAddress))
                                        Text = emailAddress.SmtpAddress;
                                }
                                break;
                        }
                    }
                }

                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Request.QueryString[SessionKey] != null)
                    {
                        Text = HttpContext.Current.Request.QueryString[SessionKey];
                    }

                    if (string.IsNullOrEmpty(Text) && HttpContext.Current.Session[SessionKey] != null)
                    {
                        Text = HttpContext.Current.Session[SessionKey].ToString();
                    }
                }
            }

            if (_isHidden)
            {
                Attributes["style"] = "display:none";
            }

            if (this.MaxLength != 0)
                return;
            this.MaxLength = 256;
        }
    }
}