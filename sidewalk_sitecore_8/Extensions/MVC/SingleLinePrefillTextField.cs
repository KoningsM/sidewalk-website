﻿using System.Collections.Generic;
using Sitecore.Data.Items;
using System.Linq;
using System.Web;
using Sitecore.Analytics;
using Sitecore.Analytics.Model.Entities;
using Sitecore.Modules.EmailCampaign.Recipients;

namespace sidewalk_sitecore_8.Extensions.MVC
{
    public class SingleLinePrefillTextField : Sitecore.Forms.Mvc.Models.Fields.SingleLineTextField
    {
        public string SessionKey { get; set; }

        public SingleLinePrefillTextField(Item item) : base(item)
        {
            Initialize();
        }

        private void Initialize()
        {
            KeyValuePair<string, string> isHidden =
                ParametersDictionary.FirstOrDefault(x => x.Key.ToUpper() == "ISHIDDEN");

            if (!string.IsNullOrWhiteSpace(SessionKey))
            {
                if (Tracker.Current != null)
                {
                    var personalFacet = Tracker.Current.Contact.GetFacet<IContactPersonalInfo>("Personal");
                    var emailFacet = Tracker.Current.Contact.GetFacet<IContactEmailAddresses>("Emails");
                    if (personalFacet != null)
                    {
                        switch (SessionKey)
                        {
                            case "firstname":
                                Value = personalFacet.FirstName;
                                break;
                            case "lastname":
                                Value = personalFacet.Surname;
                                break;
                            case "jobtitle":
                                Value = personalFacet.JobTitle;
                                break;
                            case "email":
                                if (!string.IsNullOrEmpty(emailFacet.Preferred))
                                {
                                    IEmailAddress emailAddress = emailFacet.Entries[emailFacet.Preferred];
                                    if (!string.IsNullOrWhiteSpace(emailAddress.SmtpAddress))
                                        Value = emailAddress.SmtpAddress;
                                }
                                break;
                        }
                    }
                }

                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Request.QueryString[SessionKey] != null)
                    {
                        Value = HttpContext.Current.Request.QueryString[SessionKey];
                    }

                    if (Value == null && HttpContext.Current.Session[SessionKey] != null)
                    {
                        Value = HttpContext.Current.Session[SessionKey].ToString();
                    }
                }

                if (isHidden.Value != null && isHidden.Value.ToUpper() != "NO")
                {
                    if (!string.IsNullOrWhiteSpace(CssClass))
                    {
                        CssClass += " hidden";
                    }
                    else
                    {
                        CssClass = "hidden";
                    }
                }
            }

            if (this.MaxLength != 0)
                return;
            this.MaxLength = 256;
        }
    }
}
