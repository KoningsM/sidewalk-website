﻿using System.Collections.Generic;
using System.Linq;
using sidewalk_sitecore_8.Pipelines;
using Sitecore.Diagnostics;
using Sitecore.Modules.EmailCampaign.Core.Personalization;
using Sitecore.Modules.EmailCampaign.Recipients;

namespace sidewalk_sitecore_8.Extensions
{
    public class DefaultRecipientPropertyTokenMap : RecipientPropertyTokenMap
    {
        private static readonly Dictionary<Token, RecipientPropertyTokenBinding> TokenBindings;

        static DefaultRecipientPropertyTokenMap()
        {
            RecipientPropertyTokenBinding[] bindingArray = new RecipientPropertyTokenBinding[] { RecipientPropertyTokenBinding.Build<Pipelines.PersonalInfo>(new Token("fullname"), personalInfo => personalInfo.FullName),
                RecipientPropertyTokenBinding.Build<Pipelines.PersonalInfo>(new Token("name"), personalInfo => personalInfo.FirstName),
                RecipientPropertyTokenBinding.Build<Pipelines.PersonalInfo>(new Token("firstname"), personalInfo => personalInfo.FirstName),
                RecipientPropertyTokenBinding.Build<Pipelines.PersonalInfo>(new Token("lastname"), personalInfo => personalInfo.LastName),
                RecipientPropertyTokenBinding.Build<Pipelines.PersonalInfo>(new Token("jobtitle"), personalInfo => personalInfo.JobTitle),
                RecipientPropertyTokenBinding.Build<Email>(new Token("email"), email => email.EmailAddress),
                RecipientPropertyTokenBinding.Build<Phone>(new Token("phone"), phone => phone.PhoneNumber),
                RecipientPropertyTokenBinding.Build<ExmContactInfo>(new Token("salutation"), x => x.Salutation),
                RecipientPropertyTokenBinding.Build<ExmContactInfo>(new Token("companyname"), x => x.CompanyName)
            };
            TokenBindings = (from b in bindingArray
                             orderby b.Token.Key
                             select b).ToDictionary<RecipientPropertyTokenBinding, Token, RecipientPropertyTokenBinding>(b => b.Token, t => t);
        }

        public override RecipientPropertyTokenBinding GetTokenBinding(Token token)
        {
            RecipientPropertyTokenBinding binding;
            Assert.ArgumentNotNull(token, "token");
            TokenBindings.TryGetValue(token, out binding);
            return binding;
        }

        public override IList<RecipientPropertyTokenBinding> GetTokenBindings()
        {
            return TokenBindings.Values.ToList<RecipientPropertyTokenBinding>();
        }
    }
}