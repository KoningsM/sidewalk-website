﻿using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Xml;
using System.Collections.Specialized;
using System.Xml;

namespace sidewalk_sitecore_8.SitemapXML
{
    public class SitemapManagerConfiguration
    {
        public static StringDictionary GetSites()
        {
            StringDictionary sites = new StringDictionary();
            foreach (XmlNode node in Factory.GetConfigNodes("sitemapVariables/sites/site"))
            {
                if (!string.IsNullOrEmpty(XmlUtil.GetAttribute("name", node)) && !string.IsNullOrEmpty(XmlUtil.GetAttribute("filename", node)))
                {
                    if (!sites.ContainsKey(XmlUtil.GetAttribute("name", node)))
                        sites.Add(XmlUtil.GetAttribute("name", node), XmlUtil.GetAttribute("filename", node));
                }

            }
            return sites;
        }

        public static Database WebDatabase => Factory.GetDatabase("web");

        public static string EnabledTemplates
        {
            get
            {
                var excludedTemplates = string.Empty;
                foreach (XmlNode node in Factory.GetConfigNodes("sitemapVariables/includedTemplates/includedTemplate"))
                {
                    if (!string.IsNullOrEmpty(XmlUtil.GetAttribute("value", node)))
                    {
                        excludedTemplates += $"|{XmlUtil.GetAttribute("value", node)}";
                    }
                }

                return excludedTemplates.Trim('|');
            }
        }

        public static string ExcludeItems { get; set; }

        public static string XmlnsTpl
        {
            get
            {
                var xmlns = string.Empty;
                foreach (XmlNode node in Factory.GetConfigNodes("sitemapVariables/sitemapVariable"))
                {
                    if (XmlUtil.GetAttribute("name", node) == "xmlnsTpl")
                    {
                        xmlns = XmlUtil.GetAttribute("value", node);
                        break;
                    }
                }
                return xmlns;
            }
        }

        public static string GetServerUrlBySite(string name)
        {
            var result = string.Empty;

            foreach (XmlNode node in Factory.GetConfigNodes("sitemapVariables/sites/site"))
            {
                if (XmlUtil.GetAttribute("name", node) == name)
                {
                    result = XmlUtil.GetAttribute("serverUrl", node);
                    break;
                }
            }

            return result;
        }
    }
}