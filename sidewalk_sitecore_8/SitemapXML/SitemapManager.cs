﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.Sites;
using Constants = sidewalk_sitecore_8.Utils.Constants;

namespace sidewalk_sitecore_8.SitemapXML
{
    public class SitemapManager
    {
        private static StringDictionary _mSites;
        public void RefreshSitemap(object sender, EventArgs args)
        {
            _mSites = SitemapManagerConfiguration.GetSites();
            foreach (DictionaryEntry site in _mSites)
            {
                Log.Debug("SitemapManager Site Name " + site.Key + " Value " + site.Value);
                BuildSiteMap(site.Key.ToString(), site.Value.ToString());
            }
        }

        private void BuildSiteMap(string sitename, string sitemapUrlNew)
        {
            var site = SiteManager.GetSite(sitename);
            var siteContext = Factory.GetSite(sitename);
            var rootPath = siteContext.StartPath;

            var items = GetSitemapItems(rootPath);


            var fullPath = MainUtil.MapPath(string.Concat("/", sitemapUrlNew));
            var xmlContent = BuildSitemapXml(items, site);

            var strWriter = new StreamWriter(fullPath, false);
            strWriter.Write(xmlContent);
            strWriter.Close();

        }

        private string BuildSitemapXml(IEnumerable<Item> items, Site site)
        {
            var doc = new XmlDocument();

            var declarationNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(declarationNode);
            var urlsetNode = doc.CreateElement("urlset");
            var xmlnsAttr = doc.CreateAttribute("xmlns");
            xmlnsAttr.Value = SitemapManagerConfiguration.XmlnsTpl;
            urlsetNode.Attributes.Append(xmlnsAttr);

            doc.AppendChild(urlsetNode);


            foreach (var itm in items)
            {
                var languages = itm.Languages;

                foreach (var language in languages)
                {
                    using (new LanguageSwitcher(language))
                    {
                        var it = itm.Database.GetItem(itm.ID, language);
                        if (it.Versions.Count > 0)
                        {
                            doc = BuildSitemapItem(doc, it, site);
                        }
                    }
                }
            }

            return doc.OuterXml;
        }

        private XmlDocument BuildSitemapItem(XmlDocument doc, Item item, Site site)
        {

            string url = HttpUtility.HtmlEncode(GetItemUrl(item, site));
            string lastMod = HttpUtility.HtmlEncode(item.Statistics.Updated.ToString("yyyy-MM-ddTHH:mm:sszzz"));

            var urlsetNode = doc.LastChild;

            var urlNode = doc.CreateElement("url");
            urlsetNode.AppendChild(urlNode);

            var locNode = doc.CreateElement("loc");
            urlNode.AppendChild(locNode);

            locNode.AppendChild(doc.CreateTextNode(url));

            var lastmodNode = doc.CreateElement("lastmod");
            urlNode.AppendChild(lastmodNode);
            lastmodNode.AppendChild(doc.CreateTextNode(lastMod));

            return doc;
        }

        private string GetItemUrl(Item item, Site site)
        {
            var options = UrlOptions.DefaultOptions;

            options.SiteResolving = Settings.Rendering.SiteResolving;
            options.Site = SiteContext.GetSite(site.Name);
            options.AlwaysIncludeServerUrl = false;

            if (item.Paths.LongID.Contains(Constants.BlogPostsBucketId.ToString()))
            {
                var blogpage = SitemapManagerConfiguration.WebDatabase?.GetItem(Constants.BlogPageId);
                if (blogpage != null)
                {
                    return $"{SitemapManagerConfiguration.GetServerUrlBySite(site.Name).Trim('/')}/{LinkManager.GetItemUrl(blogpage, options).Trim('/')}/{item.Name}";
                }
            }

            var url = LinkManager.GetItemUrl(item, options);

            return $"{SitemapManagerConfiguration.GetServerUrlBySite(site.Name).Trim('/')}/{url.Trim('/')}";

        }

        private IEnumerable<Item> GetSitemapItems(string rootPath)
        {
            var disTpls = SitemapManagerConfiguration.EnabledTemplates;
            var exclNames = SitemapManagerConfiguration.ExcludeItems;
            Log.Debug("GetSitemapItems RootPath" + rootPath);
            Log.Debug("exclNames + " + exclNames);
            Log.Debug("DisTpls " + disTpls);

            var enabledTemplateStrings = BuildListFromString(disTpls, '|');
            var enabledTemplates = enabledTemplateStrings.Select(templateString => new ID(templateString)).ToList();

            var index = ContentSearchManager.GetIndex(Constants.WebIndex);

            using (var context = index.CreateSearchContext())
            {
                var language = Context.Language.ToString();
                var items = context.GetQueryable<SearchResultItem>()
                    .Where(item => item.Language.Equals(language))
                    .Where(item => item.Paths.Contains(ItemIDs.ContentRoot))
                    .Where(item => item.ItemId != Constants.BlogWildcardItemId)
                    .Where(item => enabledTemplates.Contains(item.TemplateId));

                var result = items.GetResults();
                var resultItemList = result.Hits.Select(h => h.Document.GetItem()).ToList().Where(item => item != null).ToList();

                return resultItemList;
            }
        }

        private List<string> BuildListFromString(string str, char separator)
        {
            if (!string.IsNullOrEmpty(str))
            {
                var enabledTemplates = str.Split(separator);
                var selected = from dtp in enabledTemplates
                               where !string.IsNullOrEmpty(dtp)
                               select dtp;

                var result = selected.ToList();

                return result;
            }

            return new List<string>();
        }
    }
}