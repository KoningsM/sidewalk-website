﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.SearchTypes;

namespace sidewalk_sitecore_8.Indexing
{
    public class BlogPostResultItem : SearchResultItem
    {
        [IndexField("Tags")]
        public string Tags { get; set; }

        [IndexField("Author")]
        public string Author { get; set; }

        [IndexField("SortingDate")]
        public string SortingDate { get; set; }
    }
}