﻿using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using System.Web;

namespace sidewalk_sitecore_8.Models
{
    public class LinkIconModel : RenderingModel
    {
        private string _icon;
        public string Icon
        {
            get
            {
                _icon = FieldRenderer.Render(DataSource ?? Rendering.Item, "Icon");
                return _icon;
            }
            set { _icon = value; }
        }

        private HtmlString _link;
        public HtmlString Link
        {
            get
            {
                _link = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Link", "class=icon " + Icon));
                return _link;
            }
            set { _link = value; }
        }

        public Item DataSource { get; set; }
    }
}