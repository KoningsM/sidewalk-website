﻿using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using System.Web;

namespace sidewalk_sitecore_8.Models
{
    public class FeaturedProjectBlockModel : RenderingModel
    {
        private HtmlString _title;
        public HtmlString Title
        {
            get
            {
                _title = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Title"));
                return _title;
            }
            set { _title = value; }
        }

        private HtmlString _body;
        public HtmlString Body
        {
            get
            {
                _body = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Body"));
                return _body;
            }
            set { _body = value; }
        }

        public HtmlString Image => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Image", "class=screen"));

        private HtmlString _link;
        public HtmlString Link
        {
            get
            {
                _link = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Link", "class= btn btn-primary icon icon-eyeglasses"));
                return _link;
            }
            set { _link = value; }
        }

        public Item DataSource { get; set; }
    }
}