﻿using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Resources.Media;
using Constants = sidewalk_sitecore_8.Utils.Constants;

namespace sidewalk_sitecore_8.Models
{
    public class JumbotronModel : RenderingModel
    {
        public string BackgroundImageUrl
        {
            get
            {
                var options = new MediaUrlOptions { AlwaysIncludeServerUrl = true };
                var image = DataSource != null
                    ? DataSource.Fields[Constants.BackgroundImageFieldId]
                    : Rendering.Item.Fields[Constants.BackgroundImageFieldId];
                var url = string.Empty;

                if (((ImageField)image).MediaItem != null)
                {
                    url = MediaManager.GetMediaUrl(((ImageField)image).MediaItem, options);
                }

                return url;
            }
        }

        public string SectionCssClass
        {
            get
            {
                var cssClass = string.Empty;
                var sectionColorSelection = RenderingContext.Current.Rendering.Parameters["Section Color"];

                if (sectionColorSelection != null)
                {
                    var colorItem = Context.Database.GetItem(sectionColorSelection);
                    if (colorItem != null)
                    {
                        cssClass = colorItem["Value"];
                    }
                }

                var additionalCss = RenderingContext.Current.Rendering.Parameters["Additional CSS Class"];

                return $"{cssClass} {additionalCss}";
            }
        }

        public string SectionId => RenderingContext.Current.Rendering.Parameters["Section Id"];

        public Item DataSource { get; set; }
    }
}