﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using System.Collections.Generic;
using System.Linq;

namespace sidewalk_sitecore_8.Models
{
    public class ScreenshotListModel : RenderingModel
    {
        public List<Item> ScreenshotItems
        {
            get
            {
                var datasourceItem = DataSource ?? Rendering.Item;
                return ((MultilistField)datasourceItem.Fields["Screenshot list"]).GetItems().ToList();
            }
        }

        public Item DataSource { get; set; }
    }

}