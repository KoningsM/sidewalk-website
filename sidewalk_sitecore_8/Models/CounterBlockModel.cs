﻿using System.Web;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;

namespace sidewalk_sitecore_8.Models
{
    public class CounterBlockModel : RenderingModel
    {
        private HtmlString _title;
        public HtmlString Title
        {
            get
            {
                _title = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Title"));
                return _title;
            }
            set { _title = value; }
        }

        private HtmlString _icon;
        public HtmlString Icon
        {
            get
            {
                _icon = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Icon"));
                return _icon;
            }
            set { _icon = value; }
        }
        
        private HtmlString _count;
        public HtmlString Count
        {
            get
            {
                _count = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Count"));
                return _count;
            }
            set { _count = value; }
        }


        public Item DataSource { get; set; }
    }
}