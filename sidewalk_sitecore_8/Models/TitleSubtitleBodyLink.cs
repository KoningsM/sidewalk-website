﻿using System.Web;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;

namespace sidewalk_sitecore_8.Models
{
    public class TitleSubtitleBodyLink : RenderingModel
    {
        private HtmlString _title;
        public HtmlString Title
        {
            get
            {
                _title = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Title"));
                return _title;
            }
            set { _title = value; }
        }

        private HtmlString _subtitle;
        public HtmlString SubTitle
        {
            get
            {
                _subtitle = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Sub Title"));
                return _subtitle;
            }
            set { _subtitle = value; }
        }

        private HtmlString _body;
        public HtmlString Body
        {
            get
            {
                _body = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Body"));
                return _body;
            }
            set { _body = value; }
        }

        private HtmlString _link;
        public HtmlString Link
        {
            get
            {
                _link = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Link", "class=btn btn-lg btn-darkblue"));
                return _link;
            }
            set { _link = value; }
        }

        private string _icon;
        public string Icon
        {
            get
            {
                _icon = FieldRenderer.Render(DataSource ?? Rendering.Item, "Icon");
                return _icon;
            }
            set { _icon = value; }
        }

        public Item DataSource { get; set; }
    }
}