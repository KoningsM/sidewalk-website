﻿using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sidewalk_sitecore_8.Models
{
    public class ContactBlockModel : RenderingModel
    {
        private HtmlString _title;
        public HtmlString Title
        {
            get
            {
                _title = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Title"));
                return _title;
            }
            set { _title = value; }
        }

        private HtmlString _body;
        public HtmlString Body
        {
            get
            {
                _body = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Body"));
                return _body;
            }
            set { _body = value; }
        }

        public List<Item> ContactInfoList
        {
            get
            {
                var datasourceItem = DataSource ?? Rendering.Item;
                return ((MultilistField)datasourceItem.Fields["ContactInfoList"]).GetItems().ToList();
            }
        }

        public string SectionCssClass
        {
            get
            {
                var cssClass = string.Empty;
                var sectionColorSelection = RenderingContext.Current.Rendering.Parameters["Section Color"];

                if (sectionColorSelection != null)
                {
                    var colorItem = Context.Database.GetItem(sectionColorSelection);
                    if (colorItem != null)
                    {
                        cssClass = colorItem["Value"];
                    }
                }

                var additionalCss = RenderingContext.Current.Rendering.Parameters["Additional CSS Class"];

                return $"{cssClass} {additionalCss}";
            }
        }
        public Item DataSource { get; set; }
    }
}