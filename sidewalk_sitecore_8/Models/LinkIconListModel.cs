﻿using System.Linq;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Mvc.Presentation;

namespace sidewalk_sitecore_8.Models
{
    public class LinkIconListModel : RenderingModel
    {
        public List<Item> LinkIcons
        {
            get
            {
                var datasourceItem = DataSource ?? Rendering.Item;
                return ((MultilistField)datasourceItem.Fields["Social Media Items"]).GetItems().ToList();
            }
        }

        public Item DataSource { get; set; }
    }
}