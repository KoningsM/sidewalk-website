﻿using System.Collections.Generic;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Mvc.Presentation;
using Constants = sidewalk_sitecore_8.Utils.Constants;

namespace sidewalk_sitecore_8.Models.Blog
{
    public class BlogOverviewModel : RenderingModel
    {
        public List<BlogPostModel> Blogs { get; set; }

        public int TotalBlogPosts { get; set; }

        public string GetBlogLink()
        {
            var blogpage = Context.Database.GetItem(Constants.BlogPageId);

            return blogpage != null ? LinkManager.GetItemUrl(blogpage) : string.Empty;
        }
    }
}