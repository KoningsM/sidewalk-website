﻿using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sidewalk_sitecore_8.Models.Blog
{
    public class BlogMenuModel : RenderingModel
    {
        public HtmlString SiteLogo => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Image"));
        public HtmlString BlogLogo => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Blog"));

        public HtmlString MobileSiteLogo => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "MobileLogo"));
        public string SectionCssClass
        {
            get
            {
                var cssClass = string.Empty;
                var sectionColorSelection = RenderingContext.Current.Rendering.Parameters["Section Color"];

                if (sectionColorSelection != null)
                {
                    var colorItem = Context.Database.GetItem(sectionColorSelection);
                    if (colorItem != null)
                    {
                        cssClass = colorItem["Value"];
                    }
                }

                var additionalCss = RenderingContext.Current.Rendering.Parameters["Additional CSS Class"];

                return $"{cssClass} {additionalCss}";
            }
        }

        public List<Item> LinkIcons
        {
            get
            {
                var datasourceItem = DataSource ?? Rendering.Item;
                return ((MultilistField)datasourceItem.Fields["Social Media Items"]).GetItems().ToList();
            }
        }

        public Item DataSource { get; set; }
    }
}