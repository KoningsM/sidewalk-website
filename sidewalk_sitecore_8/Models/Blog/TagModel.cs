﻿using System.Web;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.Mvc.Presentation;

namespace sidewalk_sitecore_8.Models.Blog
{
    public class TagModel : RenderingModel
    {
        public HtmlString Name => new HtmlString(FieldRenderer.Render(Rendering.Item, "Name"));
    }
}