﻿using System.Web;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;

namespace sidewalk_sitecore_8.Models.Blog
{
    public class AuthorModel : RenderingModel
    {
        public HtmlString Name => new HtmlString(FieldRenderer.Render(Rendering.Item, "Name"));

        public HtmlString TwitterHandle => new HtmlString(FieldRenderer.Render(Rendering.Item, "Twitter handle"));

        public HtmlString Description => new HtmlString(FieldRenderer.Render(Rendering.Item, "Description"));

        public HtmlString Image => new HtmlString(FieldRenderer.Render(Rendering.Item, "Image"));
    }
}