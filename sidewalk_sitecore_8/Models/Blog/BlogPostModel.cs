﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Mvc.Presentation;
using Sitecore.Security.AccessControl;
using Sitecore.Web.UI.WebControls;
using Constants = sidewalk_sitecore_8.Utils.Constants;

namespace sidewalk_sitecore_8.Models.Blog
{
    public class BlogPostModel : RenderingModel
    {
        public HtmlString Title => new HtmlString(FieldRenderer.Render(DatasourceItem ?? Rendering.Item, "Title"));

        public HtmlString Intro => new HtmlString(FieldRenderer.Render(DatasourceItem ?? Rendering.Item, "Intro"));

        public HtmlString Text => new HtmlString(FieldRenderer.Render(DatasourceItem ?? Rendering.Item, "Text"));

        public HtmlString Image => new HtmlString(FieldRenderer.Render(DatasourceItem ?? Rendering.Item, "Image", "class=screen"));

        public HtmlString Date => Context.PageMode.IsPageEditor
            ? new HtmlString(FieldRenderer.Render(DatasourceItem ?? Rendering.Item, "Date"))
            : new HtmlString(DateUtil.IsoDateToDateTime(DatasourceItem != null ? DatasourceItem["Date"] : Rendering.Item["Date"]).ToString("dd/MM/yyyy"));

        public string Link
        {
            get
            {
                var blogpage = Context.Database.GetItem(Constants.BlogPageId);
                if (blogpage != null)
                {
                    return $"{LinkManager.GetItemUrl(blogpage)}/{(DatasourceItem != null ? DatasourceItem.Name : Rendering.Item.Name)}";
                }

                return string.Empty;
            }
        }

        public List<Item> Tags
        {
            get
            {
                //TODO Fieldname veranderen met ID
                var datasourceItem = DatasourceItem ?? Rendering.Item;
                return ((MultilistField)datasourceItem.Fields["Tags"]).GetItems().ToList();
            }
        }

        public Item Author => DatasourceItem != null
            ? ((ReferenceField) DatasourceItem.Fields["Author"]).TargetItem
            : ((ReferenceField)Rendering.Item.Fields["Author"]).TargetItem;

        public Item DatasourceItem { get; set; }

        public string GetTagUrl(string tag)
        {
            var overviewPage = Context.Database.GetItem(Constants.BlogPageId);
            if (overviewPage != null)
            {
                var overviewUrl = LinkManager.GetItemUrl(overviewPage);

                return $"{overviewUrl}?{Constants.TagQueryString}={tag.ToLower()}";
            }

            return string.Empty;
        }

        public string GetAuthorUrl(string author)
        {
            var overviewPage = Context.Database.GetItem(Constants.BlogPageId);
            if (overviewPage != null)
            {
                var overviewUrl = LinkManager.GetItemUrl(overviewPage);

                return $"{overviewUrl}?{Constants.AuthorQueryString}={author.ToLower()}";
            }

            return string.Empty;
        }
    }
}