﻿using System.Web;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;

namespace sidewalk_sitecore_8.Models
{
    public class ImageLinkModel : RenderingModel
    {
        public HtmlString Link => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Link"));

        public HtmlString Image => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Image"));

        public Item DataSource { get; set; }
    }
}