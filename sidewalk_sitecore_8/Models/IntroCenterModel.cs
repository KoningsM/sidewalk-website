﻿using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using System.Web;

namespace sidewalk_sitecore_8.Models
{
    public class IntroCenterModel : RenderingModel
    {
        public HtmlString Image => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Image", "class=screen"));

        public HtmlString Subtitle => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Subtitle"));

        public HtmlString Link => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Link", "class = goto icon big-icon icon-arrow-down"));

        public Item DataSource { get; set; }
    }
}