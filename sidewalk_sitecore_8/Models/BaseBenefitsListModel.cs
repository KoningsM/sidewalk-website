﻿using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FieldRenderer = Sitecore.Web.UI.WebControls.FieldRenderer;

namespace sidewalk_sitecore_8.Models
{
    public class BaseBenefitsListModel : RenderingModel
    {
        public HtmlString Title => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Title"));

        public HtmlString Link => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Link"));

        public List<Item> Benefits
        {
            get
            {
                //TODO Fieldname veranderen met ID
                var datasourceItem = DataSource ?? Rendering.Item;
                return ((MultilistField)datasourceItem.Fields["Benefits"]).GetItems().ToList();
            }
        }

        public Item DataSource { get; set; }
    }
}