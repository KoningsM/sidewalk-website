﻿using System.Collections.Generic;
using System.Linq;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;

namespace sidewalk_sitecore_8.Models
{
    public class CounterBlockListModel : RenderingModel
    {
        public List<Item> CounterBlocks
        {
            get
            {
                var datasourceItem = DataSource ?? Rendering.Item;
                return ((MultilistField)datasourceItem.Fields["Counter Blocks"]).GetItems().ToList();
            }
        }

        public Item DataSource { get; set; }
    }
}