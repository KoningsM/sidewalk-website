﻿using System;
using System.Web;
using Sitecore.Collections;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using Sitecore.Web.UI.XslControls;

namespace sidewalk_sitecore_8.Models
{
    public class TitleBodyIconModel : RenderingModel
    {
        private HtmlString _title;
        public HtmlString Title
        {
            get
            {
                _title = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Title"));
                return _title;
            }
            set { _title = value; }
        }

        private HtmlString _body;
        public HtmlString Body
        {
            get
            {
                _body = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Body"));
                return _body;
            }
            set { _body = value; }
        }

        private HtmlString _icon;
        public HtmlString Icon
        {
            get
            {
                _icon = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Icon"));
                return _icon;
            }
            set { _icon = value; }
        }
        
        public Item DataSource { get; set; }
    }
}