﻿using System.Web;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;

namespace sidewalk_sitecore_8.Models
{
    public class FreeTextModel : RenderingModel
    {
        public HtmlString Body => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Body"));

        public Item DataSource { get; set; }
    }
}