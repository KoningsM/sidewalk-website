﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;

namespace sidewalk_sitecore_8.Models
{
    public class FeaturedProjectListModel : RenderingModel
    {
        private HtmlString _title;
        public HtmlString Title
        {
            get
            {
                _title = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Title"));
                return _title;
            }
            set { _title = value; }
        }

        public List<Item> FeaturedProjectBlockModels
        {
            get
            {
                var datasourceItem = DataSource ?? Rendering.Item;
                return ((MultilistField)datasourceItem.Fields["Featured Projects"]).GetItems().ToList();
            }
        }

        public Item DataSource { get; set; }
    }
}