﻿using sidewalk_sitecore_8.Utils;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Resources.Media;

namespace sidewalk_sitecore_8.Models
{
    public class ClientLogoModel : RenderingModel
    {
        public Item DataSource { get; set; }

        private string _imageUrl;
        public string ImageUrl
        {
            get
            {
                var options = new MediaUrlOptions { AlwaysIncludeServerUrl = true };
                var image = DataSource != null
                    ? DataSource.Fields[Constants.BaseImageFieldId]
                    : Rendering.Item.Fields[Constants.BaseImageFieldId];
                _imageUrl = string.Empty;

                if (((ImageField)image).MediaItem != null)
                {
                    _imageUrl = MediaManager.GetMediaUrl(((ImageField)image).MediaItem, options);
                }

                return _imageUrl;
            }
            set { _imageUrl = value; }
        }
    }
}