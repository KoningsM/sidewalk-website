﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;

namespace sidewalk_sitecore_8.Models
{
    public class ClientLogoListModel : RenderingModel
    {
        private HtmlString _title;
        public HtmlString Title
        {
            get
            {
                _title = new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Title"));
                return _title;
            }
            set { _title = value; }
        }

        public List<Item> ClientLogoModels
        {
            get
            {
                var datasourceItem = DataSource ?? Rendering.Item;
                return ((MultilistField)datasourceItem.Fields["Logo list"]).GetItems().ToList();
            }
        }

        public Item DataSource { get; set; }
    }
}