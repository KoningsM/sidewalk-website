﻿using System.Collections.Generic;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Constants = sidewalk_sitecore_8.Utils.Constants;

namespace sidewalk_sitecore_8.Models
{
    public class SocialSharingLinksModel : RenderingModel
    {
        public List<Item> SocialSharingLinks
        {
            get
            {
                var socialSharingFolder = Context.Database.GetItem(Constants.SocialSharingFolderId);
                return socialSharingFolder != null ? new List<Item>(socialSharingFolder.GetChildren()) : new List<Item>();
            }
        }

        public string TextAlign
        {
            get
            {
                var textAlign = string.Empty;
                var textAlignSelection = RenderingContext.Current.Rendering.Parameters["Text Align"];

                if (textAlignSelection != null)
                {
                    var colorItem = Context.Database.GetItem(textAlignSelection);
                    if (colorItem != null)
                    {
                        textAlign = colorItem["CssClass"];
                    }
                }
                
                return textAlign;
            }
        }
    }
}