﻿using System.Web;
using Sitecore.Web.UI.WebControls;

namespace sidewalk_sitecore_8.Models
{
    public class BenefitsListModel : BaseBenefitsListModel
    {
        public HtmlString Body => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Body"));
    }
}