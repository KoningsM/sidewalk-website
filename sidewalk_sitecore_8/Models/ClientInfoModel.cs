﻿using System.Web;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;

namespace sidewalk_sitecore_8.Models
{
    public class ClientInfoModel : RenderingModel
    {
        public HtmlString Title => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Title"));

        public HtmlString Body => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Body"));

        public HtmlString Link => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Link"));

        public HtmlString Image => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Image"));

        public HtmlString Address => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Address"));

        public Item DataSource { get; set; }
    }
}