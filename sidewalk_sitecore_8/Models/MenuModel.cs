﻿using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sidewalk_sitecore_8.Models
{
    public class MenuModel : RenderingModel
    {
        public HtmlString SiteLogo => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Image"));

        public HtmlString MobileSiteLogo => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "MobileLogo"));

        public List<Item> MenuItems
        {
            get
            {
                var datasourceItem = DataSource ?? Rendering.Item;
                return ((MultilistField)datasourceItem.Fields["Menu Items"]).GetItems().ToList();
            }
        }

        public string CssClass
        {
            get
            {
                var cssClass = string.Empty;
                var sectionColorSelection = RenderingContext.Current.Rendering.Parameters["Section Color"];

                if (sectionColorSelection != null)
                {
                    var colorItem = Context.Database.GetItem(sectionColorSelection);
                    if (colorItem != null)
                    {
                        cssClass = colorItem["Value"];
                    }
                }

                var additionalCss = RenderingContext.Current.Rendering.Parameters["Additional CSS Class"];

                return $"{cssClass} {additionalCss}";
            }
        }
        public Item DataSource { get; set; }
    }
}