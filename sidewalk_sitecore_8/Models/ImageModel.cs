﻿using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;
using System.Web;

namespace sidewalk_sitecore_8.Models
{
    public class ImageModel : RenderingModel
    {
        public Item DataSource { get; set; }

        public HtmlString Image => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Image", "class=screen"));
    }
}