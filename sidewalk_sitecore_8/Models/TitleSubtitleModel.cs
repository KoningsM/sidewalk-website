﻿using System.Web;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using Sitecore.Web.UI.WebControls;

namespace sidewalk_sitecore_8.Models
{
    public class TitleSubtitleModel : RenderingModel
    {
        public HtmlString Title => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Title"));

        public HtmlString SubTitle => new HtmlString(FieldRenderer.Render(DataSource ?? Rendering.Item, "Sub Title"));

        public Item DataSource { get; set; }
    }
}