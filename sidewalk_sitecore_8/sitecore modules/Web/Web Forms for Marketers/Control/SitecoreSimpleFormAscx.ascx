﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SitecoreSimpleFormAscx.ascx.cs" Inherits="Sitecore.Form.Web.UI.Controls.SitecoreSimpleFormAscx" %>
<%@ Register Namespace="Sitecore.Form.Web.UI.Controls" Assembly="Sitecore.Forms.Core" TagPrefix="wfm" %>

<div class="form-container">
    <wfm:FormTitle ID="title" runat="server" />
    <wfm:FormIntroduction ID="intro" runat="server" />
    <asp:ValidationSummary ID="summary" runat="server" ValidationGroup="submit" CssClass="scfValidationSummary" />
    <wfm:SubmitSummary ID="submitSummary" runat="server" CssClass="scfSubmitSummary" />
    <div class="form-group">
        <asp:Panel ID="fieldContainer" runat="server" />
    </div>
    <wfm:FormFooter ID="footer" runat="server" />
    <div class="form-group">
        <wfm:FormSubmit ID="submit" runat="server" Class="scfSubmitButtonBorder" />
    </div>
</div>
