﻿using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.Script.Services;
using System.Web.Services;
using sidewalk_sitecore_8.Indexing;
using sidewalk_sitecore_8.Models.Blog;
using sidewalk_sitecore_8.Utils;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.SearchTypes;
using static System.String;

namespace sidewalk_sitecore_8.Webservices
{
    /// <summary>
    /// Summary description for BlogService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class BlogService : WebService
    {
        [WebMethod]
        public string GetNextBlogPosts(string itemsToSkipString, string tagName, string authorName)
        {
            int itemsToSkip;
            int.TryParse(itemsToSkipString, out itemsToSkip);

            var index = Sitecore.Context.PageMode.IsPageEditor ? ContentSearchManager.GetIndex(Constants.MasterIndex) : ContentSearchManager.GetIndex(Constants.WebIndex);
            using (var context = index.CreateSearchContext())
            {
                var bucketId = Constants.BlogPostsBucketId;
                
                var author = context.GetQueryable<SearchResultItem>()
                    .Where(item => item.TemplateId == Constants.BlogAuthorTemplateId)
                    .FirstOrDefault(item => item["Name"] == authorName.Replace("%20", " "));
                var authorId = author?.ItemId.ToGuid().ToString("n");

                var tag = context.GetQueryable<SearchResultItem>()
                    .Where(item => item.TemplateId == Constants.BlogTagTemplateId)
                    .FirstOrDefault(item => item["Name"] == tagName);
                var tagId = tag?.ItemId.ToGuid().ToString("n");

                var blogpostItems = context.GetQueryable<BlogPostResultItem>()
                    .Where(item => item.Paths.Contains(bucketId))
                    .Where(item => item.TemplateId == Constants.BlogPostTemplateId);

                if (!IsNullOrEmpty(tagId))
                {
                    blogpostItems = blogpostItems.Where(item => item.Tags.Contains(tagId));
                }

                if (!IsNullOrEmpty(authorId))
                {
                    blogpostItems = blogpostItems.Where(item => item.Author == authorId);
                }

                blogpostItems = blogpostItems
                    .OrderByDescending(item => item.SortingDate)
                    .Skip(itemsToSkip).Take(3);

                var result = blogpostItems.GetResults();
                var resultItemList = result.Hits.Select(h => h.Document).ToList();

                var blogposts = resultItemList.Select(blogpostitem => new BlogPostModel { DatasourceItem = blogpostitem.GetItem() }).ToList();

                var i = itemsToSkip + 1;
                var stringResult = new StringBuilder();
                foreach (var blogpost in blogposts)
                {
                    var invertClass = i % 2 == 0 ? "invert" : Empty;

                    var blogpostHtml = $"<li class=\"{invertClass}\">" +
                                       "<div class=\"post\">" +
                                       $"<div class=\"post-media\"><a href=\"{blogpost.Link}\">{blogpost.Image}</a></div>" +
                                       "<div class=\"post-content\">" +
                                       $"<h4 class=\"title\"><a href=\"{blogpost.Link}\">{blogpost.Title}</a></h4>" +
                                       $"<div class=\"post-desc\">{blogpost.Intro}</div>" +
                                       "<div class=\"post-info\">";

                    if (!IsNullOrEmpty(blogpost.Date.ToString()))
                    {
                        blogpostHtml += $"<div class=\"date\"><i class=\"icon icon-clock\"></i>{blogpost.Date}</div>";
                    }
                    if (blogpost.Author != null && !IsNullOrEmpty(blogpost.Author["Name"]))
                    {
                        blogpostHtml +=
                            $"<div class=\"author\"><i class=\"icon icon-user\"></i>{blogpost.Author["Name"]}</div>";
                    }

                    blogpostHtml += "</div>" +
                                    "</div>" +
                                    "</div>" +
                                    "</li>";

                    stringResult.Append(blogpostHtml);
                    i++;
                }

                return stringResult.ToString();
            }
        }
    }
}
