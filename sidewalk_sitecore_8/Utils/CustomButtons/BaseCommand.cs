﻿using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Links;
using Sitecore.Shell.Applications.WebEdit.Commands;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Sites;
using Sitecore.Web;
using Sitecore.Web.UI.Sheer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace sidewalk_sitecore_8.Utils.CustomButtons
{
    public class BaseCommand : WebEditCommand
    {
        public override void Execute(CommandContext context)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Determine if the command button should be displayed or hidden.
        /// </summary>
        public CommandState QueryState(CommandContext context, ID templateID, ID otherTemplateId)
        {

            if (context.Items.Length > 0 &&
                  context.Items[0] != null &&
                context.Items[0].TemplateID == templateID || (!otherTemplateId.IsNull && otherTemplateId == context.Items[0].TemplateID))
            {
                if (WebUtil.GetQueryString("mode") != "edit")
                {
                    return CommandState.Disabled;
                }
                return CommandState.Enabled;
            }

            return CommandState.Hidden;
        }

        /// <summary>
        /// Determine if the command button should be displayed or hidden, based on the context item's parent TemplateId
        /// </summary>
        public CommandState QueryState(CommandContext context, ID templateId)
        {

            if (context.Items.Length > 0 &&
                  context.Items[0] != null &&
                context.Items[0].Template != null && (!templateId.IsNull && context.Items[0].Template.BaseTemplates.FirstOrDefault(bt => bt.ID == templateId) != null))
            {
                if (Sitecore.Context.PageMode.IsPageEditor)
                {
                    return CommandState.Enabled;
                }

                return CommandState.Disabled;
            }

            return CommandState.Hidden;
        }

        /// <summary>
        /// Determine if the command button should be displayed or hidden.
        /// </summary>
        public CommandState QueryState(CommandContext context, List<ID> templateIds)
        {

            if (context.Items.Length > 0 && context.Items[0] != null &&
                templateIds.Contains(context.Items[0].TemplateID))
            {
                if (Context.PageMode.IsPageEditor)
                {
                    return CommandState.Enabled;
                }

                return CommandState.Disabled;
            }

            return CommandState.Hidden;
        }


        public void CreateItemAndRedirect(Item itemRoot, string itemName, ID templateId, string pageSite, Database db, Language language, ID overviewItemId)
        {
            Client.Site.Notifications.Disabled = true;
            var newItem = itemRoot.Add(itemName, new TemplateID(templateId));
            Client.Site.Notifications.Disabled = false;

            newItem.Editing.BeginEdit();
            newItem.Fields["Title"].Value = itemName;
            newItem.Editing.EndEdit();

            UrlOptions defaultOptions = UrlOptions.DefaultOptions;
            string siteName = string.IsNullOrEmpty(pageSite) ? Sitecore.Web.WebEditUtil.SiteName : pageSite;
            SiteContext site = SiteContext.GetSite(siteName);
            if (site == null)
            {
                return;
            }
            string str = string.Empty;
            using (new SiteContextSwitcher(site))
            {
                var item = db.GetItem(overviewItemId);
                using (new LanguageSwitcher(language))
                {
                    str = string.Format("{0}/{1}", LinkManager.GetItemUrl(item, defaultOptions), itemName);
                }
            }


            SheerResponse.Eval(string.Format("window.top.location.href = '{0}';", str));
        }
    }
}