﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Web.UI.Sheer;
using System;
using System.Linq;

namespace sidewalk_sitecore_8.Utils.CustomButtons
{
    public class AddItem : BaseCommand
    {
        public override void Execute(CommandContext context)
        {
            if (context.Items.Length == 1)
            {
                var item = context.Items[0];
                var parameters = new System.Collections.Specialized.NameValueCollection();
                parameters["language"] = item.Language.ToString();
                parameters["database"] = item.Database.Name;
                parameters["queryTemplateId"] = item.TemplateID.ToString();
                Sitecore.Context.ClientPage.Start(this, "Run", parameters);
            }
        }
        protected void Run(Sitecore.Web.UI.Sheer.ClientPipelineArgs args)
        {
            if (args.IsPostBack)
            {
                if ((!String.IsNullOrEmpty(args.Result)) && args.Result != "undefined" && args.Result != "null")
                {
                    var db = Sitecore.Configuration.Factory.GetDatabase(args.Parameters["database"]);

                    var itemName = args.Result;
                    var validName = ItemUtil.ProposeValidItemName(itemName);
                    var language = LanguageManager.GetLanguage(args.Parameters["language"]);
                    var queryTemplateId = new ID(args.Parameters["queryTemplateId"]);
                    Item rootItem = null;
                    var templateItemId = ID.Null;

                    GetRootFolderItemAndTemplateId(queryTemplateId, db, language, out rootItem, out templateItemId);

                    if (rootItem == null)
                    {
                        return;
                    }

                    var exist = rootItem.GetChildren().FirstOrDefault(item => item.Name == validName) != null;
                    if (!exist)
                    {
                        rootItem.Add(validName, new TemplateID(templateItemId));
                        SheerResponse.Eval("alert('The item has been created')");
                    }
                    else
                    {
                        SheerResponse.Eval("alert('The item already exist')");
                    }
                }
            }
            else
            {
                Sitecore.Context.ClientPage.ClientResponse.Input("Item name:", "(Item name)");
                args.WaitForPostBack();
            }
        }
        private void GetRootFolderItemAndTemplateId(ID queryTemplateId, Database db, Language language, out Item rootItem, out ID newItemTemplateId)
        {
            rootItem = null;
            newItemTemplateId = ID.Null;

            if (queryTemplateId == Constants.BenefitsListTemplateId)
            {
                rootItem = db.GetItem(Constants.BenefitsRootId, language);
                newItemTemplateId = Constants.BenefitsTemplateId;
            }

            if (queryTemplateId == Constants.MenuListTemplateId)
            {
                rootItem = db.GetItem(Constants.MenuListRootId, language);
                newItemTemplateId = Constants.MenuItemTemplateId;
            }

            if (queryTemplateId == Constants.CounterBlockListTemplateId)
            {
                rootItem = db.GetItem(Constants.CounterBlockListRootId, language);
                newItemTemplateId = Constants.CounterBlockTemplateId;
            }

            if (queryTemplateId == Constants.FeaturedProjectsListTemplateId)
            {
                rootItem = db.GetItem(Constants.FeaturedProjectsListRootId, language);
                newItemTemplateId = Constants.FeaturedProjectTemplateId;
            }

            if (queryTemplateId == Constants.ClientLogoListTemplateId)
            {
                rootItem = db.GetItem(Constants.ClientLogosRootId, language);
                newItemTemplateId = Constants.ClientLogoTemplateId;
            }

            if (queryTemplateId == Constants.ContactInfoListTemplateId)
            {
                rootItem = db.GetItem(Constants.ContactInfoListRootId, language);
                newItemTemplateId = Constants.ContactInfoBlockTemplateId;
            }

            if (queryTemplateId == Constants.SocialMediaItemListTemplateId)
            {
                rootItem = db.GetItem(Constants.SocialMediaItemRootId, language);
                newItemTemplateId = Constants.SocialMediaItemTemplateId;
            }

            if (queryTemplateId == Constants.ContentPageTemplateId)
            {
                rootItem = db.GetItem(Constants.BlogPostsBucketId, language);
                newItemTemplateId = Constants.BlogPostTemplateId;
            }
        }

    }
}