﻿using System;
using System.IO;
using System.Web.Mvc;
using System.Web.UI;
using Sitecore.Web.UI.WebControls;

namespace sidewalk_sitecore_8.Utils
{
    public class EditFrameRendering : IDisposable
    {
        private readonly EditFrame _editFrame;
        private readonly HtmlTextWriter _htmlWriter;

        public EditFrameRendering(TextWriter writer, string dataSource, string buttons)
        {
            _htmlWriter = new HtmlTextWriter(writer);
            _editFrame = new EditFrame { DataSource = dataSource, Buttons = buttons };
            _editFrame.RenderFirstPart(_htmlWriter);
        }
       
        public void Dispose()
        {
            _editFrame.RenderLastPart(_htmlWriter);
            _htmlWriter.Dispose();
        }
    }
}