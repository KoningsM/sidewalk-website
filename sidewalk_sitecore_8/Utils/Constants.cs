﻿using Sitecore.Data;

namespace sidewalk_sitecore_8.Utils
{
    public static class Constants
    {
        #region Base

        /* Title */
        public static ID TitleFieldId = new ID("{045D02FB-4607-4AD4-A2A5-51DD475B6E63}");

        /* Body */
        public static ID BodyFieldId = new ID("{07522D82-0656-4D43-9FE0-8EFB9C819AB3}");

        /* Link */
        public static ID LinkFieldId = new ID("{B919E2DE-E0E3-48FF-89EE-1694AE44E579}");

        /* Icon */
        public static ID IconFieldId = new ID("{FFFD698D-9658-4708-8B1E-DCD5538DD54D}");

        /* Image  */
        public static ID BaseImageFieldId = new ID("{FD4341A4-E6ED-47E3-9CC1-A5235ABB6D70}");

        #endregion


        # region Blocks

        /* Jumbotron */
        public static ID BackgroundImageFieldId = new ID("{CD60DDB9-1697-4ACB-AB11-8C262B46FE59}");

        /* Benefits list */
        public static ID BenefitsListFieldId = new ID("{95209353-C4A5-4E6E-A10D-30A613C8007B}");
        public static ID BenefitsListTemplateId = new ID("{a2db9971-9534-4d1e-9e7e-cc51c25debd9}");
        public static ID BenefitsRootId = new ID("{14441B48-5B79-493D-ADE9-297B3A6D615C}");
        public static ID BenefitsTemplateId = new ID("{5D612213-C404-41A4-AB8A-F1E44783C5B8}");

        /* Counter block list */
        public static ID CounterBlockListFieldId = new ID("{6BF49C67-9012-447F-AA63-2FD103C8DFE6}");
        public static ID CounterBlockListTemplateId = new ID("{22185D08-ABE6-4DD2-8E9C-3AA7B2E7F389}");
        public static ID CounterBlockListRootId = new ID("{7175F39E-A04D-407B-A51F-11FC69481BEB}");
        public static ID CounterBlockTemplateId = new ID("{E9FC7986-81A4-4E49-90B9-219ADCF41CF2}");

        /* Featured projects list */
        public static ID FeaturedProjectsListFieldId = new ID("{312FBAFE-AD95-4D36-8983-48BE886484D7}");
        public static ID FeaturedProjectsListTemplateId = new ID("{F674619B-0A1D-4092-999D-8C39E73F735E}");
        public static ID FeaturedProjectsListRootId = new ID("{BA1A9E4A-9074-43CE-99A4-93CC6832726C}");
        public static ID FeaturedProjectTemplateId = new ID("{C6955F89-0F55-4AF9-B962-693491BD0E7A}");

        /* Client logo list */
        public static ID ClientLogoListFieldId = new ID("{8E8F9498-30FF-4A41-9D73-9875ED2A2930}");
        public static ID ClientLogoListTemplateId = new ID("{3D1AABD2-13A2-4E09-8383-DFBA0417AC14}");
        public static ID ClientLogosRootId = new ID("{BEB4E6FF-3CDC-4866-8243-A671B1491C5F}");
        public static ID ClientLogoTemplateId = new ID("{6161F13B-384E-47A6-A2A2-2323AF3408B1}");

        /* Contact Info List*/
        public static ID ContactInfoListFieldId = new ID("{DE9CE70A-938C-4B17-AFAE-C32171C8B74F}");
        public static ID ContactInfoListTemplateId = new ID("{966F6DF8-B025-4B84-87E0-9E0E0BE51C25}");

        /* Contact info block */
        public static ID ContactInfoBlockTemplateId = new ID("{C4ED70A1-C812-4928-96CC-9258D8A53DC2}");
        public static ID ContactInfoListRootId = new ID("{0D52605D-5DE4-4D7F-AF3F-CCD060EEEFCD}");

        /* Social Media Item List */
        public static ID SocialMediaItemListFieldId = new ID("{03C014FF-205F-4727-A859-9A0314DEEB17}");
        public static ID SocialMediaItemListTemplateId = new ID("{39548103-C07E-4AD0-97D9-81E11347A7D9}");
        public static ID SocialMediaItemRootId = new ID("{05DF0337-A193-4A93-9C24-3A9FADE8EF53}");
        public static ID SocialMediaItemTemplateId = new ID("{6F9A4468-50EA-44CD-9A95-6E5C41D21005}");

        /* Menu List*/
        public static ID MenuListTemplateId = new ID("{9ce15160-e00a-4c2c-8424-44df91298c12}");
        public static ID MenuListRootId = new ID("{B178EE5E-00C5-42A5-B09D-2FD6DE2DD63F}");
        public static ID MenuItemTemplateId = new ID("{B3E4C6E7-C0D8-406E-A1FF-89F90C8AD4F5}");

        /* COntent Page*/
        public static ID ContentPageTemplateId = new ID("{b20cf1d3-4cce-447a-b178-e36b046b5281}");
        #endregion


        #region Nodes

        public static ID SidewalkRootId = new ID("{110D559F-DEA5-42EA-9C1C-8A5DF7E70EF9}");
        public static ID ContentRootId = new ID("{0DE95AE4-41AB-4D01-9EB0-67441B7C2450}");

        public static ID SidewalkWFFMFolderId = new ID("{F1F7AAB6-C8CE-422F-A214-F610C109FA63}");

        #endregion


        #region Blog

        public static ID BlogPostsBucketId = new ID("{8D5A9DEC-637C-4315-894D-DF22B6D9B276}");
        public static ID BlogPostTemplateId = new ID("{65D5DDE6-9B2A-412B-9354-33F5022B05BE}");
        public static ID BlogPageId = new ID("{90D28EE4-88A6-49C0-AB00-B5C838C4E76F}");
        public static ID BlogPageTemplateId = new ID("{FB030E4B-F228-42AD-AC49-4CC446F212B1}");
        public static ID BlogWildcardItemId = new ID("{9E6C7CCF-2268-418D-8053-3879796DE4C6}");

        public static ID BlogTagTemplateId = new ID("{C7BA8381-0728-4972-871F-93E766F6258A}");
        public static ID BlogAuthorTemplateId = new ID("{95E5C953-E622-416A-ACAE-7EA18B4E4D5E}");

        #endregion


        #region Index

        public static string WebIndex = "sitecore_web_index";
        public static string MasterIndex = "sitecore_master_index";

        #endregion


        #region Querystrings

        public static string AuthorQueryString = "author";
        public static string TagQueryString = "tag";

        #endregion

        #region Social Sharing
        public static ID SocialSharingFolderId = new ID("{D5179AE3-0987-46BA-A947-A151982C7AE9}");

        #endregion
    }
}