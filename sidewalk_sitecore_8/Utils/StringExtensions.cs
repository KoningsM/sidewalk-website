﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace sidewalk_sitecore_8.Utils
{
    public static class StringExtensions
    {
        public static string RemoveHtml(this string content)
        {
            if (content == null)
            {
                throw new NullReferenceException("RemoveHtml - the content string cannot be null");
            }
            return Regex.Replace(content, "<[^\\s].*?>", string.Empty);
        }
        public static string TruncateHtml(this string content, int length, string suffixCharaters = "")
        {
            if (!string.IsNullOrEmpty(content))
            {
                content = content.RemoveHtml();
                if (content.Length > length)
                {
                    content = content.Substring(0, content.Substring(0, length).LastIndexOf(" ", StringComparison.Ordinal)) + suffixCharaters;
                }
            }
            return content;
        }

    }
}