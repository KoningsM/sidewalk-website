﻿using Sitecore;
using Sitecore.Pipelines.RenderField;

namespace sidewalk_sitecore_8.Pipelines
{
    public class FixLinkFieldTitle
    {
        public void Process(RenderFieldArgs args)
        {
            if ((args.FieldTypeKey == "link" || args.FieldTypeKey == "general link") &&
                 args.Parameters.ContainsKey("DisableLinkDescription") &&
                 MainUtil.GetBool(args.Parameters["DisableLinkDescription"], false))
            {
                int indexOfClosingTag = args.Result.FirstPart.LastIndexOf('>');
                if (indexOfClosingTag > 0 && args.Result.FirstPart.Length > indexOfClosingTag + 1)
                    args.Result.FirstPart = args.Result.FirstPart.Remove(indexOfClosingTag + 1);
            }
        }
    }
}