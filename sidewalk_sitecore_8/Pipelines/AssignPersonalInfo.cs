﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Analytics.Model.Entities;
using Sitecore.Analytics.Model.Framework;
using Sitecore.Diagnostics;
using Sitecore.Modules.EmailCampaign.Core.Pipelines.GetXdbContactRecipient;
using Sitecore.Modules.EmailCampaign.Recipients;

namespace sidewalk_sitecore_8.Pipelines
{
    public class AssignPersonalInfo
    {
        public string PersonalInfoFacetName { get; set; }

        public void Process(GetXdbContactRecipientPipelineArgs args)
        {
            Assert.ArgumentNotNull((object)args, "args");
            if (!args.IsPropertyRequested<PersonalInfo>())
                return;
            try
            {
                IContactPersonalInfo facet = args.SourceContact.GetFacet<IContactPersonalInfo>(this.PersonalInfoFacetName);
                if (facet != null)
                {
                    string[] strArray = new string[3]
                    {
                        facet.FirstName,
                        facet.MiddleName,
                        facet.Surname
                    };
                    PersonalInfo personalInfo = new PersonalInfo()
                    {
                        FullName = string.Join(" ", Enumerable.Select<string, string>(Enumerable.Where<string>((IEnumerable<string>)strArray, (Func<string, bool>)(n => !string.IsNullOrWhiteSpace(n))), (Func<string, string>)(n => n.Trim()))),
                        FirstName = string.IsNullOrWhiteSpace(facet.FirstName) ? (string)null : facet.FirstName.Trim(),
                        LastName = string.IsNullOrWhiteSpace(facet.Surname) ? (string)null : facet.Surname.Trim(),
                        JobTitle = string.IsNullOrWhiteSpace(facet.JobTitle) ? (string)null : facet.JobTitle.Trim()
                    };
                    if (!string.IsNullOrEmpty(personalInfo.FullName) || personalInfo.FirstName != null || personalInfo.LastName != null || personalInfo.JobTitle != null)
                        args.TargetRecipient.GetProperties<PersonalInfo>().DefaultProperty = personalInfo;
                }
            }
            catch (FacetNotAvailableException)
            {
            }
        }
    }

    public class PersonalInfo : Property
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string JobTitle { get; set; }
    }
}