﻿using System;
using sidewalk_sitecore_8.Facets;
using Sitecore.Analytics.Model.Entities;
using Sitecore.Analytics.Model.Framework;
using Sitecore.Diagnostics;
using Sitecore.Modules.EmailCampaign.Core.Pipelines.GetXdbContactRecipient;
using Sitecore.Modules.EmailCampaign.Recipients;

namespace sidewalk_sitecore_8.Pipelines
{
    public class AssignSubscriberData
    {
        public void Process(GetXdbContactRecipientPipelineArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            if (args.IsPropertyRequested<ExmContactInfo>())
            {
                try
                {
                    var facet = args.SourceContact.GetFacet<ISubscriberData>(this.SubscriberDataFacet);
                    if (facet != null)
                    {
                        ExmContactInfo info = new ExmContactInfo();
                        info.Salutation = string.IsNullOrWhiteSpace(facet.Salutation) ? (string)null : facet.Salutation.Trim();
                        info.CompanyName = string.IsNullOrWhiteSpace(facet.CompanyName) ? (string)null : facet.CompanyName.Trim();

                        if (info.Salutation != null || info.CompanyName != null)
                        {
                            args.TargetRecipient.GetProperties<ExmContactInfo>().DefaultProperty = info;
                        }
                    }
                }
                catch (FacetNotAvailableException)
                {
                }
            }
        }

        public string SubscriberDataFacet { get; set; }
    }

    public class ExmContactInfo : Property
    {
        public string Salutation { get; set; }
        /*public string SidewalkSignature { get; set; }*/
        public string CompanyName { get; set; }
        // Other custom fields like ContactType, Age
    }
}