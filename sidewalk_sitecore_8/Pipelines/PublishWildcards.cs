﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sidewalk_sitecore_8.Indexing;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Form.Web.UI.Controls;
using Sitecore.Links;
using Sitecore.Publishing.Pipelines.GetItemReferences;
using Sitecore.Publishing.Pipelines.PublishItem;
using Constants = sidewalk_sitecore_8.Utils.Constants;

namespace sidewalk_sitecore_8.Pipelines
{
    public class PublishWildcards : GetItemReferencesProcessor
    {
        public Database Db => Factory.GetDatabase("master");

        protected override List<Item> GetItemReferences(PublishItemContext context)
        {
            var referenceList = new List<Item>();

            //this is the item being published
            var sourceItem = context.PublishHelper.GetSourceItem(context.ItemId);

            //this should never be true, but why not
            if (sourceItem == null || sourceItem.Empty)
                return referenceList;

            if (sourceItem.ID.Equals(Constants.BlogWildcardItemId))
            {
                var bucketId = Constants.BlogPostsBucketId;

                string indexName = $"sitecore_master_index";
                var index = ContentSearchManager.GetIndex(indexName);
                using (var searchContext = index.CreateSearchContext())
                {
                    var searchresultList = searchContext.GetQueryable<SearchResultItem>()
                            .Where(item => item.Paths.Contains(bucketId))
                            .Where(item => item.TemplateId == Constants.BlogPostTemplateId);

                    var blogposts = searchresultList.GetResults().Select(r => r.Document.GetItem());

                    referenceList.AddRange(blogposts);
                }
            }

            return referenceList;
        }

        public override void Process(PublishItemContext context)
        {
            Assert.ArgumentNotNull(context, "context");

            var sourceItem = context.PublishHelper.GetSourceItem(context.ItemId);
            if (sourceItem != null && sourceItem.Axes.IsDescendantOf(Db.GetItem(Constants.ContentRootId)))
            {
                var itemReferences = GetItemReferences(context); // this line calls the method from your custom class
                RegisterReferences(itemReferences, context); // this line registers the references to the related items.
            }
        }
    }
}
