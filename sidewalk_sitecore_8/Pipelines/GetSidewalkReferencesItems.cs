﻿using sidewalk_sitecore_8.Utils;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Globalization;
using Sitecore.Publishing.Pipelines.GetItemReferences;
using Sitecore.Publishing.Pipelines.PublishItem;
using System.Collections.Generic;
using System.Linq;

namespace sidewalk_sitecore_8.Pipelines
{
    public class GetSidewalkReferencesItems : GetItemReferencesProcessor
    {
        public Database Db
        {
            get
            {
                return Factory.GetDatabase("master");
            }
        }

        public override void Process(PublishItemContext context)
        {

            Assert.ArgumentNotNull(context, "context");

            var sourceItem = context.PublishHelper.GetSourceItem(context.ItemId);
            if (sourceItem != null && (sourceItem.Axes.IsDescendantOf(Db.GetItem(Constants.SidewalkRootId)) || sourceItem.Axes.IsDescendantOf(Db.GetItem(Constants.SidewalkWFFMFolderId))))
            {
                List<Item> itemReferences = GetItemReferences(context); // this line calls the method from your custom class

                RegisterReferences(itemReferences, context); // this line registers the references to the related items.
            }
        }



        protected override List<Item> GetItemReferences(PublishItemContext context)
        {
            var referenceList = new List<Item>();

            //this is the item being published
            var sourceItem = context.PublishHelper.GetSourceItem(context.ItemId);
            //this should never be true, but why not
            if (sourceItem == null || sourceItem.Empty)
                return referenceList;

            using (new LanguageSwitcher(sourceItem.Language))
            {
                if (sourceItem.Database != null)
                {
                    var device = DeviceItem.ResolveDevice(sourceItem.Database);
                    var renderingReference = sourceItem.Visualization.GetRenderings(device, true);
                    if (renderingReference != null)
                    {
                        var datasource = renderingReference.FirstOrDefault(o => o.RenderingID.ToString() == "{F2CCA16D-7524-4E99-8EE0-78FF6394A3B3}");
                        if (datasource != null)
                        {
                            var form = datasource.Settings.DataSource;
                            if (!string.IsNullOrEmpty(form))
                            {
                                var formItem = Db.GetItem(form);
                                if (formItem != null)
                                {
                                    referenceList.Add(formItem);

                                    referenceList.AddRange(Db.GetItem(form).Axes.GetDescendants().Where(o => o != null));
                                }
                            }
                        }
                    }
                }
            }
            return referenceList;
        }
    }
}