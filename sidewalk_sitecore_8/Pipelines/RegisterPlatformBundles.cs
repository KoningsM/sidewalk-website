﻿using Sitecore;
using Sitecore.Pipelines;
using System.Web.Optimization;

namespace sidewalk_sitecore_8.Pipelines
{
    public class RegisterPlatformBundles
    {
        [UsedImplicitly]
        public virtual void Process(PipelineArgs args)
        {
            RegisterBundles(BundleTable.Bundles);
        }

        private void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jQuery").Include(
                         "~/scripts/jquery-1.11.2.js",
                         "~/scripts/bootstrap.js",
                         "~/scripts/jquery.validate.js",
                         "~/scripts/smoothscroll.js",
                         "~/scripts/jquery.smooth-scroll.js",
                         "~/scripts/placeholders.jquery.js",
                         "~/scripts/jquery.magnific-popup.js",
                         "~/scripts/jquery.counterup.js",
                         "~/scripts/waypoint.js",
                         "~/scripts/animations/wow.js",
                         "~/scripts/jquery.jCounter-0.1.4.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                         "~/scripts/respond.js",
                         "~/scripts/custom.js"
                         ));

            bundles.Add(new ScriptBundle("~/bundles/wffmScripts").Include(
                        "~/sitecore modules/Web/Web Forms for Marketers/scripts/mvc/jquery-ui.js",
                        "~/sitecore modules/Web/Web Forms for Marketers/scripts/mvc/jquery.validate.js",
                        "~/sitecore modules/Web/Web Forms for Marketers/scripts/mvc/jquery.validate.unobtrusive.js",
                        "~/sitecore modules/Web/Web Forms for Marketers/scripts/mvc/Fields/sc.fields-unobtrusive.js",
                        "~/sitecore modules/Web/Web Forms for Marketers/scripts/mvc/Fields/sc.fields-events-tracking.js",
                        "~/sitecore modules/Web/Web Forms for Marketers/scripts/mvc/Fields/sc.fields-date.js",
                        "~/sitecore modules/Web/Web Forms for Marketers/scripts/mvc/Fields/sc.fields-captcha.js"
                        ));

            //bundles.UseCdn = true;
            //var cdnPath = "http://fonts.googleapis.com/css?family=Hind:600,300";
            //bundles.Add(new StyleBundle("~/bundles/fonts/Hind", cdnPath));

            //bundles.UseCdn = true;
            //var cdnPath2 = "http://fonts.googleapis.com/css?family=Roboto:500,100,300";
            //bundles.Add(new StyleBundle("~/bundles/fonts/Roboto", cdnPath2));

            bundles.Add(new StyleBundle("~/bundles/styles").Include(
                        "~/css/bootstrap.css",
                        "~/css/iconfont-style.css",
                        "~/css/magnific-popup.css",
                        "~/scripts/animations/animate.css",
                        "~/css/style.css",
                        "~/sitecore modules/Shell/Web Forms for Marketers/Themes/mvc/Fields/Default.css",
                        "~/sitecore modules/Shell/Web Forms for Marketers/Themes/mvc/Fields/Colors/Default.css",
                        "~/sitecore modules/Shell/Web Forms for Marketers/Themes/mvc/Fields/Custom.css",
                        "~/sitecore modules/Shell/Web Forms for Marketers/Themes/mvc/base/jquery.ui.css"
                        ));

            BundleTable.EnableOptimizations = true;
        }

    }
}