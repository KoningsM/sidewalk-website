  
$(document).ready(function () {

	"use strict";


//------------------------------------------------------------------------
	//						COUNTER SCRIPT
	//------------------------------------------------------------------------
	$('.timer').counterUp({
		delay: 20,
		time: 2500
	});


	//------------------------------------------------------------------------
	//						COUNTDOWN OPTIONS SCRIPT
	//------------------------------------------------------------------------             
	$('.countdown').jCounter({
		date: "14 february 2016 12:00:00", // Deadline date
		timezone: "Europe/Bucharest",
		format: "dd:hh:mm:ss",
		twoDigits: 'on',
		fallback: function() { console.log("count finished!") }
	});


	//------------------------------------------------------------------------
	//						NAVBAR SLIDE SCRIPT
	//------------------------------------------------------------------------ 		
	$(window).scroll(function() {
		if ($(window).scrollTop() > $("nav").height()) {
			$("nav.navbar-slide").addClass("show-menu");
		} else {
			$("nav.navbar-slide").removeClass("show-menu");
			$(".navbar-slide .navMenuCollapse").collapse({ toggle: false });
			$(".navbar-slide .navMenuCollapse").collapse("hide");
			$(".navbar-slide .navbar-toggle").addClass("collapsed");
		}
	});


	//------------------------------------------------------------------------
	//						NAVBAR HIDE ON CLICK (COLLAPSED) SCRIPT
	//------------------------------------------------------------------------ 		
	$('.nav a').on('click', function() {
		if ($('.navbar-toggle').css('display') != 'none') {
			$(".navbar-toggle").click()
		}
	});

});




$(document).ready(function () {

	"use strict";



	//------------------------------------------------------------------------
	//						ANCHOR SMOOTHSCROLL SETTINGS
	//------------------------------------------------------------------------
	$('a.goto, .navbar .nav a').smoothScroll({ speed: 1200 });




	//------------------------------------------------------------------------	
	//                    MAGNIFIC POPUP(LIGHTBOX) SETTINGS
	//------------------------------------------------------------------------  


	$('.portfolio-list li').magnificPopup({
		delegate: 'a:not(.btn, .link-item)',
		type: 'image',
		gallery: {
			enabled: true
		}
	});




	//------------------------------------------------------------------------
	//						VIDEO BACKGROUND SETTINGS
	//------------------------------------------------------------------------
	if ($('.video-bg')[0]) {
		$(function () {
			var BV = new $.BigVideo({ container: $('.video-bg'), useFlashForFirefox: false });
			BV.init();
			if (navigator.userAgent.match(/iPhone|iPad|iPod|Android|BlackBerry|IEMobile/i)) {
				BV.show('images/video_gag.jpg');
			} else {
				if (!!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0) {
					BV.show('video/video_bg.ogv', { doLoop: true, ambient: true });
				} else {
					BV.show('video/video_bg.mp4', { doLoop: true, ambient: true, altSource: 'video/video_bg.ogv' });
				}
				BV.getPlayer().on('loadedmetadata', function () {
					$('#big-video-wrap video').fadeIn('slow');
				});
			}
		});
	}




	//------------------------------------------------------------------------------------------
	//                     INITIALIZATION WOW.JS
	//------------------------------------------------------------------------------------------
	var wow = new WOW();
	wow.init();



	//------------------------------------------------------------------------
	//					SUBSCRIBE FORM VALIDATION'S SETTINGS
	//------------------------------------------------------------------------          
	$('#subscribe_form').validate({
		onfocusout: false,
		onkeyup: false,
		rules: {
			email: {
				required: true,
				email: true
			}
		},
		errorPlacement: function (error, element) {
			error.appendTo(element.closest("form"));
		},
		messages: {
			email: {
				required: "We need your email address to contact you",
				email: "Please, enter a valid email"
			}
		},

		highlight: function (element) {
			$(element)
		},

		success: function (element) {
			element
			.text('').addClass('valid')
		}
	});




	//------------------------------------------------------------------------------------
	//						SUBSCRIBE FORM MAILCHIMP INTEGRATIONS SCRIPT
	//------------------------------------------------------------------------------------		
	$('#subscribe_form').submit(function () {
		$('.error').hide();
		$('.error').fadeIn();
		// submit the form
		if ($(this).valid()) {
			$('#subscribe_submit').button('loading');
			var action = $(this).attr('action');
			$.ajax({
				url: action,
				type: 'POST',
				data: {
					newsletter_email: $('#subscribe_email').val()
				},
				success: function (data) {
					$('#subscribe_submit').button('reset');

					//Use modal popups to display messages
					$('#modalMessage .modal-title').html('<i class="icon icon-envelope-open"></i>' + data);
					$('#modalMessage').modal('show');

				},
				error: function () {
					$('#subscribe_submit').button('reset');

					//Use modal popups to display messages
					$('#modalMessage .modal-title').html('<i class="icon icon-ban"></i>Oops!<br>Something went wrong!');
					$('#modalMessage').modal('show');

				}
			});
		}
		return false;
	});




	//------------------------------------------------------------------------
	//					SUBSCRIBE 2 FIELDS FORM VALIDATION'S SETTINGS
	//------------------------------------------------------------------------          
	$('#subscribe_form_2').validate({
		onfocusout: false,
		onkeyup: false,
		rules: {
			name: "required",
			email: {
				required: true,
				email: true
			}
		},
		errorPlacement: function (error, element) {
			error.appendTo(element.closest(".validation-message"));
		},
		messages: {
			name: "What's your name?",
			email: {
				required: "We need your email address to contact you.",
				email: "Please, enter a valid email"
			}
		},

		highlight: function (element) {
			$(element)
		},

		success: function (element) {
			element
			.text('').addClass('valid')
		}
	});




	//------------------------------------------------------------------------------------
	//						SUBSCRIBE 2 FIELDS FORM MAILCHIMP INTEGRATIONS SCRIPT
	//------------------------------------------------------------------------------------		
	$('#subscribe_form_2').submit(function () {
		$('.error').hide();
		$('.error').fadeIn();
		// submit the form
		if ($(this).valid()) {
			$('#subscribe_submit_2').button('loading');
			var action = $(this).attr('action');
			$.ajax({
				url: action,
				type: 'POST',
				data: {
					newsletter_email: $('#subscribe_email_2').val(),
					newsletter_name: $('#subscribe_name_2').val()
				},
				success: function (data) {
					$('#subscribe_submit_2').button('reset');

					//Use modal popups to display messages
					$('#modalMessage .modal-title').html('<i class="icon icon-envelope-open"></i>' + data);
					$('#modalMessage').modal('show');

				},
				error: function () {
					$('#subscribe_submit').button('reset');

					//Use modal popups to display messages
					$('#modalMessage .modal-title').html('<i class="icon icon-ban"></i>Oops!<br>Something went wrong!');
					$('#modalMessage').modal('show');

				}
			});
		}
		return false;
	});




	//------------------------------------------------------------------------------------
	//						CONTACT FORM VALIDATION'S SETTINGS
	//------------------------------------------------------------------------------------		  
	$('#contact_form').validate({
		onfocusout: false,
		onkeyup: false,
		rules: {
			name: "required",
			message: "required",
			email: {
				required: true,
				email: true
			}
		},
		errorPlacement: function (error, element) {
			error.insertAfter(element);
		},
		messages: {
			name: "What's your name?",
			message: "Type your message",
			email: {
				required: "What's your email?",
				email: "Please, enter a valid email"
			}
		},

		highlight: function (element) {
			$(element)
			.text('').addClass('error')
		},

		success: function (element) {
			element
			.text('').addClass('valid')
		}
	});




	//------------------------------------------------------------------------------------
	//								CONTACT FORM SCRIPT
	//------------------------------------------------------------------------------------	
	if (document.getElementById("ContactBlock") !== null) {
	    var myElem = document.getElementById("contact_submit");
		if (myElem === null) {
			window.location.hash = 'ContactBlock';
	    }
	}




    //------------------------------------------------------------------------------------
    //								POPUP SOCIAL
    //------------------------------------------------------------------------------------	
	var popupCenter = function (url, title, w, h) {
	    // Fixes dual-screen position                         Most browsers      Firefox
	    var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
	    var dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top;

	    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	    var top = ((height / 3) - (h / 3)) + dualScreenTop;

	    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

	    // Puts focus on the newWindow
	    if (window.focus) {
	        newWindow.focus();
	    }
	};
	$('.popup').on("click", function (e) {
	    e.preventDefault();
	    var $pop = $(this);
	    popupCenter($pop.attr('href'), $pop.find('.text').html(), 580, 470);
	});
});